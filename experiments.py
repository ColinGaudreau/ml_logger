import db
import numpy as np
import learning_objects
import sqlalchemy

import pdb

class BaseExperiment(object):
	def __init__(self, learning_object, db_settings=None):
		assert(issubclass(learning_object.__class__, learning_objects.BaseLearningObject))
		self.learning_object = learning_object
		self._session = db.create_session(db_settings)
		self.experiment = None

	def train(
			self, 
			iterations,
			settings=learning_objects.BaseLearningSettings(),
			save_weights_every=10, 
			note="", 
			begin_new=False,
			save_experiment=True,
			*args,
			**kwargs
		):
		'''
		Train the learning object, save the train/test error every iteration.  Every few
		iterations save the weights of the network.

		Parameters:
		----------
		iterations - Number of iterations for which to train.
		save_weights_every - Save weights every x iterations. 
		'''
		
		self.learning_object.settings = settings

		# get experiment object
		if (begin_new or self.experiment is None):
			if save_experiment:
				self.experiment = db.Experiment(
					hyperparameters=self.learning_object.get_hyperparameters(),
					architecture=self.learning_object.get_architecture(),
					note=note
				)
				self._session.add(self.experiment)
				self._session.commit()
			self.iteration = 0
		else:
			if save_experiment:
				# save other settings
				self.experiment.hyperparameters = self.learning_object.get_hyperparameters()
				self.experiment.note = note
				self._session.commit()

		try:
			for i in range(self.iteration, self.iteration + iterations):
				train_error, test_error = self.learning_object.train(*args, **kwargs)

				if save_experiment:
					new_result = db.TrainingResult(train_error=train_error, test_error=test_error, iteration=i, experiment=self.experiment)
					self._session.add(new_result)
					self._session.commit()

					if save_weights_every is not None and (i % save_weights_every == 0 or i == iterations - 1):
						self.experiment.weights = self.learning_object.get_weights()
						self._session.commit()
		except KeyboardInterrupt:
			if save_experiment and save_weights_every is not None:
				self._session.rollback()
				self.experiment.weights = self.learning_object.get_weights()
				self._session.commit()
		self.iteration += i

	def get_train_error(self):
		results = self._session.query(db.TrainingResult).filter(db.TrainingResult.experiment == self.experiment).all()
		return np.asarray([r.train_error for r in results])

	def get_test_error(self):
		results = self._session.query(db.TrainingResult).filter(db.TrainingResult.experiment == self.experiment).all()
		return np.asarray([r.test_error for r in results])

	def load_experiment(self, experiment_id, load_model=True):
		'''
		Load experiment, and set iteration to latest one.  Load weights into learning object unless
		otherwise specified.
		'''
		self.experiment = self._session.query(db.Experiment).filter(db.Experiment.id == experiment_id).first()
		last_result = self._session.query(db.TrainingResult).filter(db.TrainingResult.experiment_id == self.experiment.id).order_by(sqlalchemy.desc(db.TrainingResult.iteration)).first()
		if last_result is None:
			self.iteration = 0
		else:
			self.iteration = last_result.iteration + 1
		
		if load_model:
			self.load_model()


	def load_model(self, experiment_id=None):
		'''
		Load weights into learning_object.
		'''
		if experiment_id != None:
			self.experiment = self._session.query(db.Experiment).filter(db.Experiment.id == experiment_id).first()
		weights = self.experiment.weights
		self.learning_object.load_model(weights)
