import pdb
import time
import numpy as np
from matplotlib import pyplot as plt

import learning_objects as lo
import experiments as exp

class NewLearningObject(lo.BaseLearningObject):
	def train(self, **kwargs):
		time.sleep(.001)
		return np.random.rand(2)
	def get_weights(self):
		return np.random.randn(10,10)
	def get_hyperparameters(self):
		return {
			'p': .5,
			'l': 1e-5
		}
	def get_architecture(self):
		return {
			'layer1': 'asdfa',
			'layer2': 'blhrjjah',
			'layer3': 'hjfdd',
			'layer4': 'another thing'
		}
	def load_model(self, weights):
		return 2 * weights

e = exp.BaseExperiment(NewLearningObject())

print('Beginning training (doing stops/starts)...')
e.train(10)
e.train(10, begin_new=False)
e.train(10, begin_new=False)
e.train(10, begin_new=False)

train_err = e.get_train_error()
test_err = e.get_test_error()

fig = plt.figure(figsize=(12,6))
plt.subplot(2,1,1)
plt.plot(train_err)
plt.subplot(2,1,2)
plt.plot(test_err)
plt.show()

print('New experiment.')
e.train(10, begin_new=True)

train_err = e.get_train_error()
fig = plt.figure(figsize=(6,6))
plt.plot(train_err)
plt.show()

print('Doing new experiment, please interrupt this one.')
e.train(1000, begin_new=True)
train_err = e.get_train_error()
fig = plt.figure(figsize=(6,6))
plt.plot(train_err)
plt.show()

print('restarting same experiment.')
e.train(10, begin_new=False)
train_err = e.get_train_error()
fig = plt.figure(figsize=(6,6))
plt.plot(train_err)
plt.show()

print('Done tests...')

# print e.load_model()

# print 'Done loading model'