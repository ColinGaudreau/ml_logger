from sqlalchemy import create_engine, ForeignKey, Column, Integer, String
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.types import JSON, PickleType, DateTime, Float
import pickle
import datetime
import sys
from settings import SETTINGS

import pdb

def create_database_connection(settings):
	if 'db' not in settings:
		raise Exception('blah')
	if 'name' not in settings:
		raise Exception('blah')
	if 'user' not in settings:
		raise Exception('blah')
	db = settings['db']
	name = settings['name']
	user = settings['user']
	if 'host' not in settings:
		host = 'localhost'
	else:
		host = settings['host']
	if 'port' not in settings:
		port = '5432'
	else:
		port = str(settings['host'])

	conn_str = db + '://' + user
	if 'password' in settings and settings['password'] is not None:
		conn_str += ":" + settings['password']

	conn_str += '@' + host + ':' + port + '/' + name

	return create_engine(conn_str)

Base = declarative_base()

class Experiment(Base):
	'''
	Model for the experiments table
	'''
	__tablename__ = 'experiments'

	id = Column(Integer, primary_key=True)
	# check if it's mysql or postgres so that I can use native json
	if 'mysql' in SETTINGS['db'] or 'postgresql' in SETTINGS['db']:
		hyperparameters = Column(JSON)
		architecture = Column(JSON)
	else:
		hyperparameters = Column(PickleType)
		architecture = Column(PickleType)
	weights = Column(PickleType(protocol=pickle.HIGHEST_PROTOCOL, pickler=pickle))
	note = Column(String)
	began = Column(DateTime, default=datetime.datetime.utcnow)

	def __repr__(self):
		return "<Experiment (id=%s), architecture=%s, hyperparameters=%s>" % (self.id, self.architecture, self.hyperparameters)

class TrainingResult(Base):
	'''
	Model for an individual training result
	'''
	__tablename__ = 'training_results'

	id = Column(Integer, primary_key=True)
	train_error = Column(Float(precision=32))
	test_error = Column(Float(precision=32))
	experiment_id = Column(Integer, ForeignKey('experiments.id'))
	iteration = Column(Integer)

	experiment = relationship("Experiment", back_populates="results")

	def __repr__(self):
		return "<TrainingResult (experiment=%s), train_error=%s, test_error=%s, iteration=%s>" % (self.experiment.id, self.train_error, self.test_error, self.iteration)

Experiment.results = relationship("TrainingResult", order_by=TrainingResult.id, back_populates="experiment")

def create_session(settings=None):
	if settings==None:
		engine = create_database_connection(SETTINGS)
	else:
		engine = create_database_connection(settings)
	Base.metadata.create_all(engine)
	Session = sessionmaker()
	Session.configure(bind=engine)
	return Session()
